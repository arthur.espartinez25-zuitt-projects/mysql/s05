--1
SELECT customerName
FROM customers
WHERE country="Philippines";
--2
SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName="La Rochelle Gifts";
--3
SELECT productName, MSRP
FROM products
WHERE productName="The Titanic";
--4
SELECT firstName, lastName
FROM employees
WHERE email="jfirrelli@classicmodelcars.com";
--5
SELECT customerName
FROM customers
WHERE state IS NULL;
--6
SELECT firstName, lastName, email
FROM employees
WHERE lastName="Patterson" AND
firstName="Steve";
--7
SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA" AND
creditLimit > 3000;
--8
SELECT customerName
FROM customers
WHERE customerName NOT LIKE "%a%";
--9
SELECT customerNumber
FROM orders
WHERE comments LIKE "%DHL%";
--10
SELECT productLine
FROM products
WHERE productDescription LIKE "%state of the art%";
--11
SELECT DISTINCT country
FROM customers;
--12
SELECT DISTINCT status
FROM orders;
--13
SELECT customerName, country
FROM customers
WHERE country="USA" OR
country="France" OR
country="Canada";
--14
SELECT firstName, lastName, city
FROM employees JOIN offices
ON employees.officeCode=offices.officeCode
WHERE offices.city="Tokyo";
--15
SELECT customerName 
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber=employees.employeeNumber
WHERE employees.lastName="Thompson" AND
employees.firstName="Leslie";
--16
SELECT productName, customerName
FROM customers 
JOIN orders ON customers.customerNumber=orders.customerNumber
JOIN orderDetails ON orders.orderNumber=orderDetails.orderNumber
JOIN products ON orderDetails.productCode=products.productCode
WHERE customerName="Baane Mini Imports";
--17
SELECT firstName, lastName, customerName, offices.country
from employees
JOIN customers ON employees.employeeNumber=customers.salesRepEmployeeNumber
JOIN offices ON employees.officeCode=offices.officeCode
WHERE customers.country=offices.country;
--18
SELECT lastName, firstName
from employees
WHERE reportsTo=1143;
--19
SELECT productName, MAX(MSRP)
from products; 
--20
SELECT COUNT(DISTINCT customerName) AS "Number of customers"
from customers
WHERE country="UK";
--21
SELECT COUNT(DISTINCT productLine) AS "Number of products"
from products;
--22
SELECT salesRepEmployeeNumber, COUNT(*)
from customers
GROUP BY salesRepEmployeeNumber;
--23
SELECT productName, quantityInStock
from products
where productLine="planes" AND
quantityInStock<1000;
